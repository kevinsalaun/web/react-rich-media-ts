import React from 'react';
import { render } from '@testing-library/react';
import App from './App';

test('Title present', () => {
  const { getByText } = render(<App />);
  const linkElement = getByText(/RichVideo/i);
  expect(linkElement).toBeInTheDocument();
});