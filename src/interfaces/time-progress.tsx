export interface CurrentChapter {
    index: number;
    title: string;
    start: number;
    end: number;
}

export interface CurrentTime {
    timeSeconds: number;
    chapter: CurrentChapter;
}

