export interface Keyword {
    pos: string;
    data: {title: string, url: string}[];
}

export interface Waypoint {
    lat: string;
    lng: string;
    label: string;
    timestamp: string;
}

export interface Chapter {
    pos: string;
    title: string;
}

export interface Video {
    title: string;
    synopsis_url: string;
    file_url: string;
    duration: number;
    chapters: Chapter[];
    waypoints: Waypoint[];
    keywords: Keyword[];
}
