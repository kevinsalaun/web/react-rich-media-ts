export interface Message {
    name: string;
    message: string;
    when: string; // unixtime of the server reception time
    moment?: string; // video time
}