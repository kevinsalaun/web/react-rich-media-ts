import SnackBar, { SnackState } from "../components/snackbar/snackbar";

export default class SnackBarService {
    private snackbarRef?: React.RefObject<SnackBar>;

    bindSnackbarRef(snackbarRef: React.RefObject<SnackBar>) {
        this.snackbarRef = snackbarRef;
    }

    public showSnackbar(snackState: SnackState, message: string) {
        if (this.snackbarRef !== undefined && this.snackbarRef.current !== undefined && this.snackbarRef.current !== null) {
            this.snackbarRef.current.handleOpen(snackState, message);
        }
        /* else {
            alert(`${snackState} - ${message}`);
        }*/
    }
}
