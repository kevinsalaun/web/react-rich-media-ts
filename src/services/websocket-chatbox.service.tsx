import { BehaviorSubject } from 'rxjs';
import { Message } from '../interfaces/message';

export default class WebsocketChatbox {
  private URL = "wss://imr3-react.herokuapp.com";
  private ws: WebSocket;

  private status = false;
  private _statusObs = new BehaviorSubject<boolean>(false);
  readonly statusObs = this._statusObs.asObservable();

  private messages: Message[] = [];
  //private _messagesObs = new BehaviorSubject<Message|undefined>(undefined);
  private _messagesObs = new BehaviorSubject<Message[]>([]);
	readonly messagesObs = this._messagesObs.asObservable();

  constructor() {
    this.ws = new WebSocket(this.URL)
    this.initWebsocket();
  }

  private initWebsocket() {
    // connect the websocket
    this.ws.onopen = () => {
      console.log("WebsocketChatbox - connected");
      this.status = true;
      this._statusObs.next(this.status);
    };

    // receive messages
    this.ws.onmessage = evt => {
      const msgList: Message[] = JSON.parse(evt.data);
      for (let msg of msgList) {
        this.messages.push(msg);
        //this._messagesObs.next(Object.assign({}, msg));
      }
      //this.messages = msgList;
      this._messagesObs.next(this.messages.slice());
      console.log("WebsocketChatbox - received messages:", this.messages);
    };

    // close socket
    this.ws.onclose = () => {
      console.log("WebsocketChatbox - disconnected, try to reconnect...");
      this.status = false;
      this._statusObs.next(this.status);
      this.ws = new WebSocket(this.URL);
      this.initWebsocket();
    };
  }

  public sendMessage(message: Message) {
    if (this.status) { this.ws.send(JSON.stringify(message)); }
  }

  public getStatus(): boolean {
    return this.status;
  }
}