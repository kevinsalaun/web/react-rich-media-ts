import React from 'react';
import { render } from '@testing-library/react';
import MovieData from "./movie-data.service";

let fetch = require('jest-fetch-mock');

beforeEach(() => {
    fetch = jest.fn().mockImplementation(()=>{
        return Promise.resolve({
            status:200,
            json: ()=> {
                return Promise.resolve([
                    {surname:"surname 1",name:"name 1",resume:"resume 1"},
                ])
            },
        })
    })
});

test("data format",()=>{ 
    expect(fetch).toHaveBeenCalledTimes(1);
    expect(fetch).toHaveBeenCalledWith('https://imr3-react-backend.herokuapp.com');
})

afterAll(()=>{
    fetch.mockClear()
})
