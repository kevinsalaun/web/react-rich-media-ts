import { BehaviorSubject } from "rxjs";
import { Video, Keyword, Waypoint, Chapter } from "../interfaces/video";
import { CurrentTime, CurrentChapter } from "../interfaces/time-progress";

export default class MovieData {
  private URL = "https://imr3-react.herokuapp.com/backend";
  private limit = 5;
  private attempt = 0;

  private currentTime: CurrentTime = {
    timeSeconds: 0,
    chapter: {
      index: 0,
      title: '',
      start: 0,
      end: 0
    }
  }
  private _cTimeObs = new BehaviorSubject<CurrentTime>(Object.assign({}, this.currentTime));
  readonly cTimeObs = this._cTimeObs.asObservable();
  private _cChapterObs = new BehaviorSubject<CurrentChapter>(Object.assign({}, this.currentTime.chapter));
  readonly cChapterObs = this._cChapterObs.asObservable();

  private video: Video|undefined = undefined;
  private _videoObs = new BehaviorSubject<Video|undefined>(undefined);
  readonly videoObs = this._videoObs.asObservable();

  private _goToObs = new BehaviorSubject<number>(0);
  readonly goToObs = this._goToObs.asObservable();

  private _readyObs = new BehaviorSubject<boolean>(false);
  readonly readyObs = this._readyObs.asObservable();

  constructor() { 
    this.loadData();
  }
  public test() {}
  private loadData() {
    // console.log('MovieData - beginning of the data video loading');
    fetch(this.URL)
      .then(res => res.json())
        .then(json => {
          const keywords: Keyword[] = json.Keywords;
          const waypoints: Waypoint[] = json.Waypoints;
          const chapters: Chapter[] = json.Chapters;
          this.video = {
            title: json.Film.title,
            file_url: json.Film.file_url,
            synopsis_url: json.Film.synopsis_url,
            duration: 0,
            keywords,
            waypoints,
            chapters
          }
          this._videoObs.next(Object.assign({}, this.video));
          console.log('MovieData - data loaded:', this.video);

          const firstChapter = this.video.chapters[0];
          const firstChapterPos = parseInt(firstChapter.pos);
          this.currentTime = {
            timeSeconds: 0,
            chapter: {
              index: 0,
              title: firstChapter.title,
              start: parseInt(firstChapter.pos),
              end: (this.video.chapters.length > 1) ? parseInt(this.video.chapters[1].pos) : firstChapterPos
            }
          }
          this._cTimeObs.next(Object.assign({}, this.currentTime));
          this._cChapterObs.next(Object.assign({}, this.currentTime.chapter));
          // console.log('MovieData - reset current time:', this.currentTime);
        })
          .catch(error => {
            console.error(error);
            if (this.attempt < this.limit) {
              console.log('MovieData - next loading attempt in 3s');
              setTimeout(() => { this.loadData(); }, 3000);
            } else {
              console.log('MovieData - loading abandoned');
              this._videoObs.error('Loading abandoned');
            }
            this.attempt++;
          });
    }

    private getChapterIndex(timeSeconds: number): number {
      if (this.video) {
        const chapters = this.video.chapters;
        let chapterIndex = -1;
        for (let i = chapters.length - 1; i >= 0 && chapterIndex === -1; i--) {
          if (parseInt(chapters[i].pos) <= timeSeconds) {
            chapterIndex = i;
          }
        }
        return (chapterIndex === -1) ? 0 : chapterIndex;
      }
      return 0;
    }

    public setDuration(timeSeconds: number) {
      if (this.video) this.video.duration = timeSeconds;
    }

    public setCurrentTime(timeSeconds: number) {
      if (this.video) {
        const previousChapter = this.currentTime.chapter.start;
        const chapterIndex = this.getChapterIndex(timeSeconds);
        const chapters = this.video.chapters;
        const chapterPos = parseInt(chapters[chapterIndex].pos);
        this.currentTime = {
          timeSeconds: timeSeconds,
          chapter: {
            index: chapterIndex,
            title: chapters[chapterIndex].title,
            start: chapterPos,
            end: (chapters.length > chapterIndex+1) ? parseInt(chapters[chapterIndex+1].pos) : this.video.duration
          }
        }
        this._cTimeObs.next(Object.assign({}, this.currentTime));
        if (previousChapter !== chapterPos) {
          this._cChapterObs.next(Object.assign({}, this.currentTime.chapter));
        }
      }
    }

    public goTo(timeSeconds: number) {
      this._goToObs.next(timeSeconds);
    }

    public playerReady() {
      this._readyObs.next(true);
    }

    public playerError() {
      this._readyObs.next(false);
    }
}
