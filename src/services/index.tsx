import MovieData from "./movie-data.service";
import WebsocketChatbox from "./websocket-chatbox.service";
import SnackBarService from "./snackbar.service";

export class Services {
    private static instance: Services;

    // The Singleton's constructor (should always be private)
    readonly movieData: MovieData;
    readonly websocketChatbox: WebsocketChatbox;
    readonly snackBarService: SnackBarService;

    private constructor() { 
        this.movieData = new MovieData();
        this.websocketChatbox = new WebsocketChatbox();
        this.snackBarService = new SnackBarService();
    }

    // The static method that controls the access to the singleton instance.
    public static getInstance(): Services {
        if (!Services.instance) {
            Services.instance = new Services();
        }
        return Services.instance;
    }
}