import React, { Component } from 'react';
import './App.scss';
import Mapview from './components/mapview/mapview.component';
import Keywords from './components/keywords/keywords.component';
import Chatbox from './components/chatbox/chatbox.component';
import RichVideo from './components/rich-video/rich-video.component';
import { Grid, Paper } from '@material-ui/core';
import SnackBarService from './services/snackbar.service';
import { Services } from './services';
import SnackBar from './components/snackbar/snackbar';

interface State {
}

interface Props {
}

export default class App extends Component<Props, State> {
  snackbarRef: React.RefObject<SnackBar>;
  snackBarService: SnackBarService;

  constructor(props: Props) {
    super(props);

    // refs
    this.snackbarRef = React.createRef();

    // connects snackbar to the dedicated service
    this.snackBarService = Services.getInstance().snackBarService;
    this.snackBarService.bindSnackbarRef(this.snackbarRef);
  }
  
  render() {
    return (
      <div className="App">
        <Grid container spacing={3}>

          <Grid item xs={12} sm={12} md={12}>
            <Paper className="paper header" square={false}>
              <h1>RichVideo</h1>
            </Paper>
          </Grid>

          <Grid container item xs={12} sm={12} md={9} spacing={3}>
            {/* <Grid container spacing={3}> */}

              <Grid item xs={12} sm={12} md={12}>
                <Paper className="paper">
                  <RichVideo></RichVideo>
                </Paper>
              </Grid>
              
              <Grid item xs={12} sm={12} md={9} >
                <Paper className="paper">
                  <h2>Localisation</h2>
                  <Mapview></Mapview>
                </Paper>
              </Grid>
              <Grid item xs={12} sm={12} md={3}>
                <Paper className="paper">
                  <h2>Mots clés</h2>
                  <Keywords></Keywords>
                </Paper>
              </Grid>

            {/* </Grid> */}
          </Grid>

          <Grid item xs={12} sm={12} md={3}>
            <Paper className="paper">
              <h2>Chatbox</h2>
              <Chatbox></Chatbox>
            </Paper>
          </Grid>

        </Grid>
        <SnackBar ref={this.snackbarRef}></SnackBar>
      </div>
    );
  }
}