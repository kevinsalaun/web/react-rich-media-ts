import React, { Component } from "react";

import { Snackbar } from "@material-ui/core";
import MuiAlert from '@material-ui/lab/Alert';

export enum SnackState {
    Success = "success",
    Info = "info",
    Warning = "warning",
    Error = "error"
}

interface State {
    snackState: SnackState;
    open: boolean;
    message: string;
}
  
interface Props {
}

export default class SnackBar extends Component<Props, State> {
    constructor(props: Props) {
        super(props);
        this.state = {
            snackState: SnackState.Info,
            open: false,
            message: ''
        }

        // keep "this"
        this.handleOpen = this.handleOpen.bind(this);
        this.handleClose = this.handleClose.bind(this);
    }

    handleOpen(snackState: SnackState, message: string) {
        this.setState({
            snackState,
            message,
            open: true,
        });
    };

    handleClose(event?: React.SyntheticEvent, reason?: string) {
        if (reason === 'clickaway') return;
        this.setState({open: false});
    };

    render() {
        return (
            <Snackbar open={this.state.open} autoHideDuration={2000} onClose={this.handleClose}>
                <MuiAlert onClose={this.handleClose} severity={this.state.snackState} elevation={6} variant="filled">
                    {this.state.message}
                </MuiAlert>
            </Snackbar>
        )
    }
}