import React, {Component} from 'react';
import './mapview.component.scss'
import { Services } from '../../services';
import { Video, Waypoint } from '../../interfaces/video';
import { Map, Marker, Popup, TileLayer, FeatureGroup } from 'react-leaflet';
import L from 'leaflet';
import MovieData from '../../services/movie-data.service';
import { CurrentChapter } from '../../interfaces/time-progress';
import AspectRatioIcon from '@material-ui/icons/AspectRatio';
import { IconButton } from '@material-ui/core';

const markerBlueIcon = L.icon({
  iconUrl: process.env.PUBLIC_URL+"/images/marker_blue.png",
  iconSize: [25, 41]
});

const markerPurpleIcon = L.icon({
  iconUrl: process.env.PUBLIC_URL+"/images/marker_purple.png",
  iconSize: [25, 41]
});

interface State {
  waypoints: Waypoint[];
  chapWaypoints: Waypoint[];
  lat: number;
  lng: number;
  zoom: number;
}

type Props = {
}

export default class Mapview extends Component<Props, State> {
  movieData: MovieData;
  mapRef: React.RefObject<Map>;
  groupRef: React.RefObject<FeatureGroup>;

  constructor(props: Props) {
    super(props);
    this.state = {
      waypoints: [],
      chapWaypoints: [],
      lat: 53.8,
      lng: 32.63,
      zoom: 4,
    };

    this.movieData = Services.getInstance().movieData;
    
    // ref
    this.mapRef = React.createRef();
    this.groupRef = React.createRef();

    // keep "this"
    this.fitMarkers = this.fitMarkers.bind(this);
  }

  componentDidMount() {
    // prevents setState from not being present
    this.movieData.videoObs.subscribe((video ?: Video) => {
      if (video) {
        this.setState({ waypoints: video.waypoints }, () => {
          this.fitMarkers();
        });
      }
    })

    this.movieData.cChapterObs.subscribe((chapter: CurrentChapter) => {
      this.setState({
        chapWaypoints: this.state.waypoints.filter((wp: Waypoint) => {
          const wpPos: number = parseInt(wp.timestamp);
          return (wpPos >= chapter.start && wpPos <= chapter.end)
        })
      });
    });
  }

  fitMarkers() {
    if (this.mapRef.current && this.groupRef.current) {
      const map = this.mapRef.current.leafletElement;
      const group = this.groupRef.current.leafletElement;
      map.fitBounds(group.getBounds());
    } else {
      setTimeout(() => this.fitMarkers(), 1000);
    }
  }

  render() {
    const listMarkers = this.state.waypoints.map((wp: Waypoint, index: number) => {
      const position = {lat: parseFloat(wp.lat), lng: parseFloat(wp.lng)};
      const active = this.state.chapWaypoints.includes(wp);
      return (
        <Marker icon={active ? markerPurpleIcon : markerBlueIcon} 
          position={position} key={index}
          opacity={active ? 1.0 : 0.5}
          onMouseOver={(e: any) => {e.target.openPopup();}}
          onMouseOut={(e: any) => {e.target.closePopup();}}
        >
          <Popup>
            <span>{wp.label}</span>
          </Popup>
        </Marker>
      )
    });

    return (
      <div className="component-mapview">
        <Map ref={this.mapRef} zoom={this.state.zoom}
          center={{ lat: this.state.lat, lng: this.state.lng}}
        >
          <TileLayer
            url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
            attribution="&copy; <a href=&quot;http://osm.org/copyright&quot;>OpenStreetMap</a> contributors"
          />
          <FeatureGroup ref={this.groupRef}>
            {listMarkers}
          </FeatureGroup>
        </Map>
        <IconButton aria-label="fit-markers" onClick={this.fitMarkers}>
          <AspectRatioIcon />
        </IconButton>
      </div>
    )
  }
}
