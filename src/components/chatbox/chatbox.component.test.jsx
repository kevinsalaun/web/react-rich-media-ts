import React from 'react';
import { waitForElement, render } from '@testing-library/react';
import Chatbox from "./chatbox.component"

beforeAll(() => {
  // We haven't the time to mock all services =)
  jest.spyOn(global, 'fetch').mockImplementation(() => {
    return Promise.resolve({
      status: 200,
      json: () => {
        return Promise.resolve([
          {
            "when": "1580742794",
            "name": "Alice",
            "message": "Hi, I'm Alice!"
          },
          {
            "when": "1580742479",
            "name": "Bob",
            "message": "Hi, I'm Bob. Checkout this moment!",
            "moment": 462
          }
        ]);
      }
    });
  });
});
afterAll(() => {
  fetch.mockClear();
});

test("renders without crashing", () => {
  render(<Chatbox />);
});

/*test("backend is called", () => {
  render(<Chatbox />);
  expect(global.fetch).toHaveBeenCalledTimes(1);
  expect(global.fetch).toHaveBeenCalledWith('https://imr3-react-backend.herokuapp.com');
});*/
