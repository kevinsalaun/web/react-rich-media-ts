import React, {Component, ChangeEvent} from 'react';
import './chatbox.component.scss'

import ChatboxMessageList from './chatbox-message-list/chatbox-message-list.component';
import { Message } from '../../interfaces/message';

import { FormControl, TextField, IconButton } from '@material-ui/core';
import SendIcon from '@material-ui/icons/Send';
import RoomIcon from '@material-ui/icons/Room';
import { Services } from '../../services';
import WebsocketChatbox from '../../services/websocket-chatbox.service';
import PseudoDialog from './pseudo-dialog/pseudo-dialog.component';
import { CurrentTime } from '../../interfaces/time-progress';
import MovieData from '../../services/movie-data.service';
import SnackBarService from '../../services/snackbar.service';
import { SnackState } from '../snackbar/snackbar';

interface State {
  messages: Message[];
  websocketStatus: boolean;
  msgInput: string;
  pseudo: string;
  openDialog: boolean;
  currentTime: CurrentTime;
  addTimestamp: boolean;
  videoServiceReady:  boolean;
  ready: boolean;
}

interface Props {
}

export default class Chatbox extends Component<Props, State> {
  websocketChatbox: WebsocketChatbox;
  movieData: MovieData;
  snackBarService: SnackBarService;

  constructor(props: Props) {
    super(props);
    this.state = {
      messages: [],
      websocketStatus: false,
      msgInput: '',
      pseudo: '',
      openDialog: false,
      currentTime: {
        timeSeconds: 0,
        chapter: {
          index: 0,
          title: '',
          start: 0,
          end: 0
        }
      },
      addTimestamp: false,
      videoServiceReady: false,
      ready: false
    };
    this.websocketChatbox = Services.getInstance().websocketChatbox;
    this.movieData = Services.getInstance().movieData;
    this.snackBarService = Services.getInstance().snackBarService;

    // propagate "this" into the function
    this.toggleTimestamp = this.toggleTimestamp.bind(this);
    this.onSend = this.onSend.bind(this);
    this.handleChangeMsg = this.handleChangeMsg.bind(this);
    this.setPseudo = this.setPseudo.bind(this);
    this.checkReady = this.checkReady.bind(this);
    this.goTo = this.goTo.bind(this);
  }

  componentDidMount() {
    this.websocketChatbox.statusObs.subscribe((status: boolean) => {
      this.setState({websocketStatus: status}, () => {
        this.checkReady();
      });
      if (status) {
        this.snackBarService.showSnackbar(SnackState.Info, 'vous êtes connecté à la chatbox');
      } else {
        this.snackBarService.showSnackbar(SnackState.Error, 'vous avez été déconnecté de la chatbox');
      }
    });
    this.websocketChatbox.messagesObs.subscribe((messages: Message[]) => {
      this.setState({messages: messages.splice(-50)});
    });

    this.movieData.cTimeObs.subscribe((ctime: CurrentTime) => {
      this.setState({currentTime: ctime});
    });
    this.movieData.readyObs.subscribe((state) => {
      this.setState({videoServiceReady: state}, () => {
        this.checkReady();
      });
    })
  }

  toggleTimestamp() {
    this.setState({addTimestamp: !this.state.addTimestamp});
  }

  onSend() {
    if (this.state.msgInput.length > 0) {
      const msg: Message = {
        name: this.state.pseudo,
        message: this.state.msgInput,
        moment: (this.state.addTimestamp) ? this.state.currentTime.timeSeconds+'' : '',
        when: ''
      }
      this.websocketChatbox.sendMessage(msg);
      this.setState({
          messages: this.state.messages || [],
          msgInput: '',
          addTimestamp: false
      });
    }
  }

  handleChangeMsg(event: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) {
    event.persist();
    this.setState({msgInput: event.target.value});
  }

  goTo(timeSeconds: number) {
    this.movieData.goTo(timeSeconds);
  }

  checkReady() {
    if (this.state.videoServiceReady && this.state.websocketStatus && this.state.pseudo.length !== 0) {
      this.setState({ready: true});
    }
  }

  setPseudo(name: string) {
    this.setState({pseudo: name}, () => {
      this.checkReady();
    });
  }

  render() {
    let pseudoState;
    if (this.state.pseudo.length !== 0) {
      pseudoState = <span>({this.state.pseudo})</span>
    } else {
      pseudoState = <span className="warning">(veuillez définir un pseudo)</span>
    }

    return (
      <div className="component-chatbox">
        <div className={this.state.ready ? undefined : 'disabled'}>
          <ChatboxMessageList ref="msglist" items={this.state.messages} 
            goTo={this.goTo} disabled={!this.state.ready} 
          />
        </div>
        <FormControl className="chat-action-area">
          <div className="row-align">
            <div>
              <TextField onChange={this.handleChangeMsg} value={this.state.msgInput} id="chat-msg-input" 
                placeholder="message" label="Message" inputProps={{maxLength: 200}} autoComplete="off"
                onKeyPress={(e) => {if(e.key === 'Enter') this.onSend() } } disabled={!this.state.ready}
              />
            </div>
            <IconButton color={(this.state.addTimestamp) ? 'secondary' : 'default'} component="span" aria-label="add-timestamp"
              onClick={(e:any) => this.toggleTimestamp()} disabled={!this.state.ready}
            >
              <RoomIcon />
            </IconButton>
            <IconButton onClick={(e) => this.onSend()} color="secondary" aria-label="send" disabled={!this.state.ready}>
              <SendIcon />
            </IconButton>
          </div>
          <PseudoDialog onValid={this.setPseudo} currentPseudo={this.state.pseudo}/> {pseudoState}
        </FormControl>
      </div>
    )
  }
}
