import React, {Component} from 'react';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';
import { Message } from '../../../interfaces/message';
import moment from 'moment';
import { IconButton } from '@material-ui/core';
import RoomIcon from '@material-ui/icons/Room';

interface State {
  messages: Message[];
  preventScroll: boolean;
  delayedPreventScroll?: ReturnType<typeof setTimeout>;
}

interface Props {
  items: Message[];
  goTo: Function;
  disabled: boolean;
}

export default class ChatboxMessageList extends Component<Props, State> {
  messagesEnd: any;
  msgRef: React.RefObject<HTMLDivElement>;

  constructor(props: Props) {
    super(props);
    this.state = {
      messages: this.props.items,
      preventScroll: false,
      delayedPreventScroll: undefined
    };

    // ref
    this.msgRef = React.createRef();

    // keep "this"
    this.goTo = this.goTo.bind(this);
    this.preventScroll = this.preventScroll.bind(this);
  }
  
  public static defaultProps = {
    items: []
  };

  componentWillReceiveProps(props: Props) {
    this.setState({messages: props.items}, () => {
      this.scrollToBottom();
    });
  }
  
  componentDidMount() {
    this.scrollToBottom();
    if (this.props.items) this.setState({messages: this.props.items});
  }
  
  /*componentDidUpdate() {
    this.scrollToBottom();
  }*/

  scrollToBottom = () => {
    if (this.msgRef.current && !this.state.preventScroll) {
      const container = this.msgRef.current;
      if (container.scrollTo !== undefined) {
        container.scrollTo({
          top: container.scrollHeight,
          behavior: 'smooth'
        });
      }
    }
  }

  preventScroll(state: boolean) {
    if (!state) {
      this.setState({delayedPreventScroll: setTimeout(() => this.setState({preventScroll: false}), 3000)});
    } else {
      this.setState({preventScroll: state});
      if (this.state.delayedPreventScroll) {
        clearTimeout(this.state.delayedPreventScroll);
        this.setState({delayedPreventScroll: undefined});
      }
    }
  }

  goTo(moment?: string) {
    if (moment !== undefined) {
      this.props.goTo(parseFloat(moment));
    }
  }

  render() {
    const listItems = this.state.messages.map((item: Message, index: number) => {
      const dateISO: string = new Date(item.when).toISOString();
      const formatedDate = moment(dateISO).format('HH:mm');
      return (
        <ListItem alignItems="flex-start" key={index}>
          <ListItemAvatar>
            <Avatar alt="item.author" src={process.env.PUBLIC_URL+"/images/default_avatar.svg"} />
          </ListItemAvatar>
          <ListItemText
            secondary={
              <React.Fragment>
                <Typography
                  component="span"
                  variant="body2"
                  color="textPrimary"
                >
                  {`${formatedDate}, ${item.name}`}

                  { item.moment !== undefined && item.moment.length !== 0 && 
                    <IconButton className="icon-mini" color="primary" size="small" component="span" 
                      onClick={(e:any) => this.goTo(item.moment)} aria-label="timestamp" disabled={this.props.disabled}
                    >
                      <RoomIcon fontSize="inherit"/>
                    </IconButton>
                  }
                </Typography>
                {" — " + item.message}
              </React.Fragment>
            }
          />
        </ListItem>
      )
    });

    return (
      <div className='component-chatbox-message-list' ref={this.msgRef} 
        onMouseDown={e => this.preventScroll(true)} onMouseUp={e => this.preventScroll(false)}
        onScroll={(e:any) => {this.preventScroll(true); this.preventScroll(false)}}
      >
        <List component="div">
          {(listItems.length) ? listItems : 'soyez le premier à poster un message'}
          <div style={{ float:"left", clear: "both" }}
            ref={(el) => { this.messagesEnd = el; }}>
          </div>
        </List>
      </div>
    )
  }
}
