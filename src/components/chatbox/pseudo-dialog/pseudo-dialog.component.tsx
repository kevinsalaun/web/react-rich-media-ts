import React, { Component, ChangeEvent } from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';

interface State {
  pseudo: string;
  openDialog: boolean;
}

interface Props {
  onValid: (pseudo: string) => void;
  currentPseudo: string;
}

export default class PseudoDialog extends Component<Props, State> {
  public static defaultProps = {
    currentPseudo: ''
  };

  constructor(props: Props) {
    super(props);
    this.state = {
      pseudo: this.props.currentPseudo,
      openDialog: false
    }

    this.handleChangePseudo = this.handleChangePseudo.bind(this);
    this.handleClickOpen = this.handleClickOpen.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.handleValidate = this.handleValidate.bind(this);
  }

  handleChangePseudo(event: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) {
    this.setState({pseudo: event.target.value.trim()});
  }

  handleClickOpen() {
    this.setState({openDialog: true});
  };

  handleClose() {
    this.setState({openDialog: false});
  };

  handleValidate() {
    if (this.state.pseudo.length !== 0) {
      this.props.onValid(this.state.pseudo);
      this.setState({openDialog: false});
    }
  }

  handleKey(e: React.KeyboardEvent<HTMLDivElement>) {
    const key = e.key;
    if (key === 'Enter') this.handleValidate();
    else if (key === 'Escape') this.handleClose();
    //e.preventDefault();
  }

  render() {
    return (
      <div>
        <Button variant="outlined" color="primary" onClick={this.handleClickOpen}>
          Définir pseudo
        </Button>
        <Dialog open={this.state.openDialog} onClose={this.handleClose} aria-labelledby="form-dialog-title">
          <DialogTitle id="form-dialog-title">Choix pseudonyme</DialogTitle>
          <DialogContent>
            {/* <DialogContentText>
              Veuillez saisir votre pseudo:
            </DialogContentText> */}
            <TextField
              autoFocus
              margin="dense"
              type="text"
              id="pseudo"
              label="Pseudo"
              placeholder="pseudo"
              fullWidth
              onChange={this.handleChangePseudo}
              inputProps={{maxLength: 12}}
              onKeyPress={(e) => { this.handleKey(e) } }
              value={this.state.pseudo}
            />
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleClose} color="primary">
              Annuler
            </Button>
            <Button onClick={this.handleValidate} color="primary" disabled={this.state.pseudo.length === 0}>
              Valider
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    )
  }
}
