import React, {Component, ChangeEvent} from 'react';
import './rich-video.component.scss'
import ReactPlayer from 'react-player'
import { Video } from '../../interfaces/video';
import { Services } from '../../services';
import MovieData from '../../services/movie-data.service';
import moment from 'moment';
import { Card, IconButton, withStyles, Slider, Popper, Grow, Paper, ClickAwayListener, MenuList, MenuItem } from '@material-ui/core';
import StopIcon from '@material-ui/icons/Stop';
import PlayArrowIcon from '@material-ui/icons/PlayArrow';
import PauseIcon from '@material-ui/icons/Pause';
import VolumeUpIcon from '@material-ui/icons/VolumeUp';
import VolumeOffIcon from '@material-ui/icons/VolumeOff';
import { CurrentChapter } from '../../interfaces/time-progress';
import SnackBarService from '../../services/snackbar.service';
import { SnackState } from '../snackbar/snackbar';

const SliderBar = withStyles({
  root: {
    color: '#52af77',
    height: 8,
  },
  thumb: {
    height: 24,
    width: 24,
    backgroundColor: '#fff',
    border: '2px solid currentColor',
    marginTop: -8,
    marginLeft: -12,
    '&:focus,&:hover,&$active': {
      boxShadow: 'inherit',
    },
  },
  active: {},
  valueLabel: {
    left: 'calc(-50% + 4px)',
  },
  track: {
    height: 8,
    borderRadius: 4,
  },
  rail: {
    height: 8,
    borderRadius: 4,
  },
})(Slider);

interface SliderMark {
  value: number;
  label: string;
}

////

interface VState {
  played: number; 
  playedSeconds: number; 
  loaded: number; 
  loadedSeconds: number;
}

interface State {
  video?: Video;
  error: string;
  playing: boolean;
  stop: boolean,
  muted: boolean,
  volume: number;
  played: number;
  playedSeconds: number; 
  duration: number;
  seeking: boolean;
  openVolume: boolean;
  volumeBarValue: number;
  currentChapterIndex: number;
  videoServiceReady: boolean;
}

interface Props {
  autoPlay: boolean
}

export default class RichVideo extends Component<Props, State> {
  playerRef: React.RefObject<ReactPlayer>;
  volumeRef: React.RefObject<HTMLButtonElement>;
  movieData: MovieData;
  snackBarService: SnackBarService;

  public static defaultProps = {
    autoPlay: false
  };

  constructor(props: Props) {
    super(props);
    this.state = {
      video: undefined,
      error: '',
      playing: false,
      stop: false,
      volume: 0.8,
      muted: false,
      played: 0.0,
      playedSeconds: 0,
      duration: 0.0,
      seeking: false,
      openVolume: false,
      volumeBarValue: 1.0,
      currentChapterIndex: 0,
      videoServiceReady: false
    };
    this.movieData = Services.getInstance().movieData;
    this.snackBarService = Services.getInstance().snackBarService;

    // refs
    this.playerRef = React.createRef();
    this.volumeRef = React.createRef();

    // keep "this"
    this.handleToggleStop = this.handleToggleStop.bind(this);
    this.handlePlayPause = this.handlePlayPause.bind(this);
    this.handlePlay = this.handlePlay.bind(this);
    this.handlePause = this.handlePause.bind(this);
    this.handleSeekChange = this.handleSeekChange.bind(this);
    this.handleSeekMouseUp = this.handleSeekMouseUp.bind(this);
    this.handleProgress = this.handleProgress.bind(this);
    this.handleDuration = this.handleDuration.bind(this);
    this.handleHideVolume = this.handleHideVolume.bind(this);
    this.handleShowVolume = this.handleShowVolume.bind(this);
    this.handleVolumeChange = this.handleVolumeChange.bind(this);
    this.showError = this.showError.bind(this);
  }

  componentDidMount() {
    // prevents setState from not being present (used in controls)
    this.movieData.videoObs.subscribe(
      (video?: Video) => {
        this.setState({
          video: video,
          volumeBarValue: 1 - this.state.volume
        });
        if (this.props.autoPlay) {
          setTimeout(this.handlePlay, 500);
        }
      },
      (err: string) => {this.setState({error: err})}
    )
    this.movieData.cChapterObs.subscribe(
      (chapter: CurrentChapter) => {
        this.setState({currentChapterIndex: chapter.index});
      }
    )
    this.movieData.goToObs.subscribe((timeSeconds: number) => {
      this.goTo(timeSeconds);
    })

    this.movieData.readyObs.subscribe((state) => {
      this.setState({videoServiceReady: state});
    })
  }

  showError(e: string) {
    this.setState({playing: false});
    if (typeof e === 'string' && e !== undefined && e != null) {
      if (e.indexOf('play() failed') !== -1) {
        console.log(e);
        setTimeout(this.handlePlay, 500);
      } else {
        console.error(e);
        this.movieData.playerError();
        this.snackBarService.showSnackbar(SnackState.Error, 'Erreur chargement vidéo');
      }
    }
  }

  ////

  handleToggleStop = () => {
    if (this.playerRef.current) {
      this.setState({ 
        played: 0.0, 
        playedSeconds: 0,
        playing: false 
      });
      this.playerRef.current.seekTo(0);
    }
  }

  handlePlayPause = () => {
    this.setState({ playing: !this.state.playing });
  }

  handlePlay = () => {
    this.setState({ playing: true });
  }

  handlePause = () => {
    this.setState({ playing: false });
  }

  handleSeekChange = (_e: ChangeEvent<{}>, value: number | number[]) => {
    if (Array.isArray(value)) {
      value = value[value.length-1];
    }
    this.setState({
      seeking: true, 
      played: value / this.state.duration,
      playedSeconds: value
    });
  }

  handleSeekMouseUp = () => {
    this.setState({ seeking: false });
    if (this.playerRef.current) {
      this.playerRef.current.seekTo(this.state.played);
    }
  }

  goTo(timeSeconds: number) {
    this.setState({
      played: timeSeconds / this.state.duration,
      playedSeconds: timeSeconds
    }, () => {
      if (this.playerRef.current) {
        this.playerRef.current.seekTo(this.state.played);
      }
    });
  }

  handleProgress = (state: VState) => {
    if (!this.state.seeking) {
      this.movieData.setCurrentTime(state.playedSeconds);
      this.setState({
        played: state.played,
        playedSeconds: state.playedSeconds,
      });
    }
  }

  handleDuration = (duration: number) => {
    this.setState({ duration });
    this.movieData.setDuration(duration);
  }

  ////

  handleHideVolume() {
    this.setState({openVolume: false});
  }

  handleShowVolume() {
    this.setState({openVolume: true});
  }

  handleVolumeChange = (_e: ChangeEvent<{}>, value: number | number[]) => {
    if (Array.isArray(value)) {
      value = value[value.length-1];
    }
    const reverseVal = 1 - value;
    this.setState({ 
      volume: reverseVal,
      volumeBarValue: value
    });
    if (reverseVal === 0) {
      this.setState({ muted: true });
    } else {
      this.setState({ muted: false });
    }

    /*handleToggleMuted = () => {
      this.setState({ muted: !this.state.muted })
    }*/
  }

  ////

  formatTime(seconds: number) {
    return moment("2000-01-01").startOf('day').seconds(seconds).format('HH:mm:ss');
  }

  getMarkers(): Array<SliderMark> {
    const marks: SliderMark[] = [];
    if (this.state.video && this.isReady()) {
      const chapters = this.state.video.chapters;
      for (let i=0; i<chapters.length; i++) {
        marks.push({
          value: parseFloat(chapters[i].pos),
          label: (i+1)+''
        });
      }
    }
    return marks;
  }

  isReady(): boolean {
    return (this.playerRef.current !== undefined && this.state.videoServiceReady);
  }

  render() {
    if (this.state.video) {
      return (
        <div className="component-rich-video">
          <ReactPlayer className='react-player'
            ref={this.playerRef}
            url={this.state.video.file_url}
            playing={this.state.playing}
            controls={false}
            volume={this.state.volume}
            muted={this.state.muted}
            playbackRate={1.0}
            loop={false}
            light={false}
            pip={false}
            onReady={() => this.movieData.playerReady()}
            // onStart={() => console.log('onStart')}
            onPlay={this.handlePlay}
            onPause={this.handlePause}
            // onBuffer={() => console.log('onBuffer')}
            // onSeek={e => console.log('onSeek', e)}
            onError={this.showError}
            onProgress={this.handleProgress}
            onDuration={this.handleDuration}
            // onEnded={() => console.log('onEnded')}
            onClick={this.handlePlayPause}
          />
          <Card className="video-controller">
            <div className='flex-row'>
              <SliderBar aria-label="timeline"
                className="timeline"
                onChange={this.handleSeekChange}
                onChangeCommitted={this.handleSeekMouseUp}
                defaultValue={0}
                min={0} max={this.state.duration} step={1}
                marks={this.getMarkers()}
                value={this.state.playedSeconds}
                disabled={!this.playerRef.current}
              />
              <span className="elapsed-time">{this.formatTime(this.state.playedSeconds)}</span>
            </div>

            <div className='flex-row center'>
              <div className='controls'>
                <IconButton aria-label="stop" onClick={this.handleToggleStop} disabled={!this.isReady()}>
                  <StopIcon />
                </IconButton>
                <IconButton aria-label="play/pause" className="play-btn" onClick={this.handlePlayPause} disabled={!this.isReady()}>
                  {this.state.playing ? (<PauseIcon fontSize="inherit"/>) : (<PlayArrowIcon fontSize="inherit"/>)}
                </IconButton>
                <IconButton aria-label="mute" onClick={this.handleShowVolume} ref={this.volumeRef} disabled={!this.isReady()}>
                  {this.state.muted ? (<VolumeOffIcon />) : (<VolumeUpIcon />)}
                </IconButton>
                <Popper className="volume-popper" open={this.state.openVolume && this.playerRef.current !== undefined} 
                  anchorEl={this.volumeRef.current} role={undefined} transition disablePortal
                >
                  {({ TransitionProps, placement }) => (
                    <Grow
                      {...TransitionProps}
                      style={{ transformOrigin: placement === 'bottom' ? 'center top' : 'center bottom' }}
                    >
                      <Paper>
                        <ClickAwayListener onClickAway={this.handleHideVolume}>
                          <MenuList id="menu-list-grow" onKeyDown={this.handleShowVolume}
                            autoFocusItem={this.state.openVolume}
                          >
                            <MenuItem>
                              <Slider className="volume-bar" orientation="vertical"
                                defaultValue={0} min={0} max={1} step={0.01}
                                track="inverted" onChange={this.handleVolumeChange} 
                                value={this.state.volumeBarValue} 
                              />
                            </MenuItem>
                          </MenuList>
                        </ClickAwayListener>
                      </Paper>
                    </Grow>
                  )}
                </Popper>
              </div>

              <div className="flex-column description">
                <div className="title">
                  {this.state.video.title}
                </div>
                <div className="subtitle">
                  "{this.state.video.chapters[this.state.currentChapterIndex].title}"
                </div>
                <div className="extra">
                  <a href={this.state.video.synopsis_url} target="_blank" rel="noopener noreferrer">synopsis</a>
                </div>
              </div>
            </div>

          </Card>
        </div>
      );
    } else if(this.state.error) {
      return (<div>Une erreur est survenue, rechargez la page ultérieurement...</div>);
    } else {
      return (<div>Loading...</div>);
    }
  }
}
