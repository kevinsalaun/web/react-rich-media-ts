import React, {Component} from 'react';
import './keywords.component.scss'
import MovieData from '../../services/movie-data.service';
import {  Keyword, Video } from '../../interfaces/video';
import { Paper, Chip } from '@material-ui/core';
import { Services } from '../../services';
import { CurrentChapter } from '../../interfaces/time-progress';

interface State {
  keywords: Keyword[];
  chapKeywords: Keyword[];
}

interface Props {
}

export default class Keywords extends Component<Props, State> {
  movieData: MovieData;
  
  constructor(props: Props) {
    super(props);
    this.state = {
      keywords: [],
      chapKeywords: []
    };
    this.movieData = Services.getInstance().movieData;
  }

  componentDidMount() {
    // prevents setState from not being present
    this.movieData.videoObs.subscribe((video ?: Video) => {
      if (video) this.setState({keywords: video.keywords});
    });

    this.movieData.cChapterObs.subscribe((chapter: CurrentChapter) => {
      this.setState({
        chapKeywords: this.state.keywords.filter((kw: Keyword) => {
          const kwPos: number = parseInt(kw.pos);
          return (kwPos >= chapter.start && kwPos <= chapter.end)
        })
      });
    });
  }

  render() {
    return (
      <Paper className='component-keywords'>
        { this.state.chapKeywords.map(kw => {
          return kw.data.map((data, index) => {
            return (
              <Chip
                component="a" 
                key={index} 
                label={data.title} 
                href={data.url}
                target="_blank"
                clickable
              />
            );
          });
        })}
      </Paper>
    );
  }
}
